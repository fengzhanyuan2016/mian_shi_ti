﻿using System;
using System.Collections.Generic;
using System.Text;

namespace longSpliter
{
    public class Spliter
    {
        public string Map(List<long> list)
        {
            string s = "";
            char index = 'A';
            List<char> chars = new List<char>();
            foreach (var item in list)
            {
                var chazhi = index - 1;
                chars.Add((char)(item+chazhi));
            }
            return string.Join(",",chars);
        }

        public List<long> Longs(long num)
        {
            var nums = new List<long>();
            long shiwei = 0;
            long before = 27;
            //比较0的情况
            while ((num % 10) >= 0)
            {
                var item = (num % 10);
                if (item != 0 && before != 0)
                {
                    nums.Add(item);
                }
                if (item <= 2 && before == 0)
                {
                    nums.Add(item * 10);
                }

                if ((item > 2 || item == 0) && before == 0)
                {
                    return new List<long>();
                }
                num = num / 10;
                if (num == 0)
                {
                    break;
                }
                before = item;
            }
            nums.Reverse();
            return nums;
        }





        //一个Root数组可以分多个子数组，递归一直分下去
        public List<List<long>> Split(List<long> nums)
        {
            var list = new List<List<long>>();
            list.Add(nums);
            for (int i = 0; i < nums.Count-1; i++)
            {
                var n = nums[i] * 10 + nums[i + 1];
                if (n <= 26)
                {
                    var temps = new List<long>(nums);
                    temps.RemoveRange(i,2);
                    temps.Insert(i,n);
                    list.AddRange(Split(temps));       
                }
            }
            return list;
        }

    }
}
