﻿using longSpliter;
using System;
using System.Collections.Generic;

namespace IntSpliter
{
    class Program
    {
        static void Main(string[] args)
        {
            Spliter sp = new Spliter();

            //Step1 寻找最长Root数组
            long a = 1234567899876543210;
            var nums = sp.Longs(a);
            if (nums.Count == 0)
            {
                Console.WriteLine("不能组成任何的字符串");
            }
            else
            {


                foreach (var item in nums)
                {
                    Console.Write($"{item} ");
                }
                Console.WriteLine("\r\n-----------------------分割线------------------------");

                //Step2 组合最长数组元素
                var result = sp.Split(nums);
                foreach (var ls in result)
                {
                    var s = sp.Map(ls);
                    Console.WriteLine(s);
                }


            }

            Console.WriteLine("Finish!");
            Console.ReadKey();
        }



    }
}
